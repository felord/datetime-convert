package cn.felord.datetime.convert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dax
 */
@SpringBootApplication
public class DatetimeConvertApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatetimeConvertApplication.class, args);
    }

}
